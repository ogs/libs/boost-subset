# Boost-Subset

- 1.69.0 for https://gitlab.opengeosys.org/ogs/ogs/-/commit/a455d3fd94b63e01ca480730b1edd9a6a3b8d7da: https://gitlab.opengeosys.org/ogs/libs/boost-subset/-/jobs/187805/artifacts/raw/ogs-boost-1.69.0.tar.gz
- 1.75.0 for https://gitlab.opengeosys.org/ogs/ogs/-/commit/a455d3fd94b63e01ca480730b1edd9a6a3b8d7da: https://gitlab.opengeosys.org/ogs/libs/boost-subset/-/jobs/187771/artifacts/raw/ogs-boost-1.75.0.tar.gz
